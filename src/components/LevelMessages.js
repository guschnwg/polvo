import React from 'react';
import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
`

const IndicatorMessageContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  margin: 0;
`

const Message = styled.span`
  font-size: 14px;
  color: #DBDBDB;
`

const Indicator = styled.div`
  height: 10px;
  width: 10px;

  margin-right: 5px;

  border-radius: 50%;

  background: ${props => props.valid !== null ? (props.valid ? '#1ED699' : '#F26722') : '#DBDBDB'};
`

export default (props) => (
  <Container direction="column">
    { props.rules.map((rule, idx) => (
      <IndicatorMessageContainer direction="row">
        <Indicator key={idx} valid={rule.valid} />
        <Message>{rule.message}</Message>
      </IndicatorMessageContainer>
    )) }
  </Container>
)
