import React from 'react';
import styled from 'styled-components';
import { validationLevel } from '../helpers/validate'

const Container = styled.div`
  display: flex;
  margin-bottom: 10px !important;

  > *:not(:last-child) {
    margin-right: 2px;
  }
  > *:not(:first-child) {
    margin-left: 2px;
  }
`

const Level = styled.div`
  height: 5px;
  width: 100%;
  background: ${props => props.color}
`
export default (props) => (
  <Container>
    { new Array(props.levels).fill(0).map((_, idx) => <Level key={idx} color={idx < props.filled ? validationLevel[props.filled] : validationLevel[0]} />) }
  </Container>
)
