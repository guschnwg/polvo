// import React from 'react';
import styled from 'styled-components';
import { validationLevel } from '../helpers/validate'

export default styled.input`
  outline: none;
  border: 1px solid #BFBFBF;

  border-color: ${props => validationLevel[props.validationLevel]}

  color: #828282;
  box-shadow: inset 1px 1px 2px rgba(0, 0, 0, 0.15);
  font-size: 14px;
  padding: 10px;
`;
