// import React from 'react';
import styled from 'styled-components';

// const Button = (props) => (
//   <button {...props}>{props.label}</button>
// )

export default styled.button`
  outline: none;
  border: 1px solid #6C41D7;

  color: white;
  background: #6C41D7;
  padding: 10px;
  font-size: 14px;

  width: 100%;
`;
