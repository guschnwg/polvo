import React from 'react';
import styled from 'styled-components';
import Input from './Input';

const FormItem = (props) => (
  <div className={props.className}>
    { props.label && <label htmlFor={props.id}>{props.label}</label> }
    <Input {...props} />
  </div>
)

export default styled(FormItem)`
  display: flex;
  flex-direction: column;

  > label {
    font-size: 14px;
    color: #828282;
    margin-bottom: 5px;
  }
`;
