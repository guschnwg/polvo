import React from 'react';
import styled from 'styled-components';

const Heading = (props) => <span className={props.className}>{props.content}</span>

export default styled(Heading)`
  font-weight: bold;
  text-align: center;
  font-size: ${props => props.size}rem;
  color: ${props => props.color};
`
