export const validationLevel = ['#BFBFBF', '#F26722', '#F2B822', '#1ED699']

export default (rules, value) => {
  return rules.map(rule => ({ ...rule, valid: validate(rule, value) }))
}

const validate = (rule, value) => {
  if (value === '') {
    return null
  }

  switch (rule.type) {
    case 'min':
      return value.length >= rule.value;
    case 'uppercase':
      return (value.match(/[A-Z]/g) || []).length > 0;
    case 'number':
      return (value.match(/[0-9]/g) || []).length > 0;
    default:
      console.log('WHAT', rule.type)
      return false;
  }
}
