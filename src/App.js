import React from 'react';
import styled from 'styled-components';
import Login from './screens/Login';

const App = (props) => (
  <div {...props}>
    <Login />
  </div>
);

export default styled(App)`
  display: flex;
  height: 100vh;
  width: 100vw;
  justify-content: center;
  align-items: center;
  background: #E5E5E5;
`;
