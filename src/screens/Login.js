import React from 'react';
import styled from 'styled-components';
import FormItem from './../components/FormItem';
import Button from './../components/Button';
import Heading from './../components/Heading';
import LevelIndicator from '../components/LevelIndicator';
import LevelMessages from '../components/LevelMessages';

import validateRules from '../helpers/validate';

import logo from './../logo.svg';

class Login extends React.Component {
  state = {
    email: '',
    password: '',
    passwordConfirm: '',
    passwordValidation: [
      {
        type: 'min',
        value: 6,
        message: 'Pelo menos 6 caracteres',
        valid: null
      },
      {
        type: 'uppercase',
        value: 1,
        message: 'Pelo menos 1 letra maiúscula',
        valid: null
      },
      {
        type: 'number',
        value: 1,
        message: 'Pelo menos 1 número',
        valid: null
      }
    ]
  }

  validate = (value) => {
    this.setState({ passwordValidation: validateRules(this.state.passwordValidation, value) })
  }

  handleInput = (field, value) => {
    this.setState({ [field]: value })

    if (field === 'password') {
      this.validate(value)
    }
  }

  render() {
    const { email, password, passwordConfirm, passwordValidation } = this.state

    let passwordRulesValid = 0
    if (password !== '') {
      passwordRulesValid = passwordValidation.map(rule => rule.valid).filter(valid => valid).length
    }

    return (
      <form className={this.props.className}>
        <img src={logo} alt="logo" />

        <Heading size={1} color="#6C41D7" content="Seja bem-vindo" />

        <FormItem label="E-mail" name="email" id="email" value={email} onChange={(event) => this.handleInput('email', event.target.value)} />

        <FormItem
          type="password"
          label="Senha"
          name="password"
          id="password"
          validationLevel={passwordRulesValid}
          value={password}
          onChange={(event) => this.handleInput('password', event.target.value)}
        />

        <LevelIndicator levels={passwordValidation.length} filled={passwordRulesValid} />
        <LevelMessages rules={passwordValidation} />

        <FormItem
          type="password"
          label="Confirmar senha"
          name="passwordConfirm"
          id="passwordConfirm"
          value={passwordConfirm}
          onChange={(event) => this.handleInput('passwordConfirm', event.target.value)}
        />

        <Button>Cadastrar</Button>
      </form>
    );
  }
}

export default styled(Login)`
  background: white;
  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.1);
  width: 320px;
  max-width: 90vw;
  max-height: 90vh;

  box-sizing: border-box;
  padding: 30px 50px;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: stretch;

  > * {
    margin-bottom: 20px;
  }
`;
